import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Person } from '../shared/models/person.model';
@Component({
  selector: 'app-person-add',
  templateUrl: './person-add.component.html',
  styleUrls: ['./person-add.component.css']
})
export class PersonAddComponent implements OnInit {
  @Output() addperson = new EventEmitter<Person>();
  FormAdd: FormGroup;
  disabled:true;
  constructor() { }

  ngOnInit() {
    this.FormAdd = new FormGroup({
      firstname: new FormControl({value: '', disabled:this.disabled}, [Validators.required]),
      lastname: new FormControl({value:'', disabled:this.disabled}, [Validators.required]),
      phone: new FormControl({value:'', disabled:this.disabled}, [Validators.required])
    })
  }

  onAddPerson(firstname:string, lastname:string, phone:string){
    if (firstname != "" && lastname != "" && phone != ""){
    let person = new Person(firstname, lastname, phone);
    this.addperson.emit(person);
    }
}
public mask = [8,'(', /[0-9]/, /[0-9]/, /[0-9]/, ')', ' ', /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/];
}

