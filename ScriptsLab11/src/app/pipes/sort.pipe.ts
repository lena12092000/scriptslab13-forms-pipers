import { Pipe, PipeTransform } from '@angular/core';
import { Person } from '../shared/models/person.model';
import { isNullOrUndefined } from 'util';
@Pipe({
  name: 'searchFirstname'
})
export class SortPipe implements PipeTransform {

  transform(persons: Person[], searchStr: string) {
    if (!isNullOrUndefined(persons) && searchStr.trim() !== "") {
      console.log(searchStr);
      let filter_persons = persons.filter(
        person => person.firstname.toLowerCase().indexOf(searchStr.toLowerCase()) === 0
      );
      return  filter_persons;
    }
    return persons;
  }
}
