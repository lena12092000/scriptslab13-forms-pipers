import { Component, OnInit, OnDestroy } from '@angular/core';
import { Person } from './shared/models/person.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  title = 'Компоненты';
  persons: Person[] = [];

  constructor(){

  }

  ngOnInit():void{
    this.persons.push(new Person('Ivan', 'Ivanov', '8(934) 253-64-75', 1));
    this.persons.push(new Person('Ivan', 'Ivanov', '8(934) 253-64-75',2));
    this.persons.push(new Person('Ivan', 'Ivanov', '8(934) 253-64-75',3));
    this.persons.push(new Person('Ivan', 'Ivanov', '8(934) 253-64-75',4));
    this.persons.push(new Person('Ivan', 'Ivanov', '8(934) 253-64-75',5));

    // this.FormFilter = new FormGroup({
    //   firstname:new FormControl({value:'', disabled:this.disabled}, [Validators.required]),
    //   lastname: new FormControl({value:'', disabled:this.disabled}, [Validators.required])
    // })
  }
  ngOnDestroy():void{

  }
  onAddPerson(person: Person){
  let newId = this.persons[this.persons.length-1].id+1;
  person.id=newId;
  this.persons.push(person);
  }
  deletePerson(id) {
   this.persons.splice(this.persons.findIndex(elem => elem.id == id), 1);
 }

 editPerson(person) {
    this.persons.splice(
      this.persons.findIndex(elem => elem.id == person.id),
      1,
      person
    );
  }
}
