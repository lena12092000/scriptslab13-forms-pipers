import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Person } from '../shared/models/person.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-person-view',
  templateUrl: './person-view.component.html',
  styleUrls: ['./person-view.component.css']
})
export class PersonViewComponent implements OnInit {
  @Input() inPerson: Person;
  @Output() delete = new EventEmitter<number>();
  @Output() edit = new EventEmitter<Person>();
  nowEdit:boolean;
  FormEdit:FormGroup;
  disabled:true;
  constructor() { }

  ngOnInit() {
    console.log(this.inPerson);
    this.nowEdit = false;
    this.FormEdit = new FormGroup({
      firstname: new FormControl({value: this.inPerson.firstname, disables:this.disabled}, [Validators.required]),
      lastname: new FormControl({value:this.inPerson.lastname, disabled:this.disabled}, [Validators.required]),
      phone: new FormControl({value:this.inPerson.phone, disabled:this.disabled}, [Validators.required])
    })
  }
  onDeletePerson() {
      this.delete.emit(this.inPerson.id);
    }

    
 onEditPerson(firstname: string = this.inPerson.firstname, lastname: string = this.inPerson.lastname, phone:string = this.inPerson.phone) {
  if (firstname !== "" && lastname !== "" && phone != "") {
    let person = new Person(firstname, lastname, phone, this.inPerson.id);
    this.edit.emit(person);
    this.Edit();
  }
 }
 Edit() {
  this.nowEdit = !this.nowEdit;
}
public mask = [8,'(', /[0-9]/, /[0-9]/, /[0-9]/, ')', ' ', /[0-9]/, /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/, '-', /[0-9]/, /[0-9]/];
}
